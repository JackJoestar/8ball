CC=gcc
CFLAGS=-c -Wall -Wextra

all: eightball

eightball: eightball.o ; $(CC) eightball.o -o eightball

eightball.o: eightball.c ; $(CC) $(CFLAGS) eightball.c

clean: ; rm -rf *o eightball
