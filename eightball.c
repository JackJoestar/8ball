#include <stdio.h>
#include <stdlib.h>
#include <time.h>

const char *lines[] = {"It is certain.",
	"It is decidedly so.",
	"Without a doubt.",
	"Yes - definitely.",
	"You may rely on it.",
	"As I see it, yes.",
	"Most likely",
	"Outlook good.",
	"Yes.",
	"Signs point to yes.",
	"Reply hazy, try again.",
	"Ask again later.",
	"Better not tell you now.",
	"Cannot predict now.",
	"Concentrate and ask again.",
	"Don't count on it.",
	"My reply is no.",
	"My sources say no.",
	"Outlook not so good.",
	"Very doubtful"};

int main(){
	srand(time(NULL));
	int r = (rand() % 20);

	printf("--------------------------------\n");
	printf("|                              |\n");
	printf("--------------------------------\n");
	printf("*%s*\n",lines[r]);
	printf("--------------------------------\n");
	printf("|                              |\n");
	printf("--------------------------------\n");
	return 0;
}
