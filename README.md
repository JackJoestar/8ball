#8ball
This is a novelty command line program which simulates the 8Ball fortune telling toy.

To compile this, type:
`make all`

To then run this, ask yourself a Yes/No question, then type:
`./eightball`